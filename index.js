const restify = require("restify");
const path = require("path");
const dotenv = require("dotenv");

const { BotFrameworkAdapter, MemoryStorage, UserState, ConversationState, ActivityTypes } = require("botbuilder");

const { MainBot } = require("./bot/mainBot");
const { UserBot } = require("./userBots/userBot");

// Read environment variables from .env file
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });

// Create the adapter. See https://aka.ms/about-bot-adapter to learn more about using information from
// the .bot file when configuring your adapter.
const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword
});

// Middleware to Send Typing activity
// adapter.use(async (context, next) => {
//     await context.sendActivities([
//         { type: ActivityTypes.Typing },
//         { type: 'delay', value: 500 },
//         { type: ActivityTypes.Message }
//     ]);
//     // Calling the next function in queue
//     await next();
// });

// define the state store for your bot
// A bot requires a state storeage system to presist the dialog and user state between conversion
const memoryStorage = new MemoryStorage();

// create Conversion state with in memory  storage provider
const conversationState = new ConversationState(memoryStorage);
const userState = new UserState(memoryStorage);


// create main dialog
const userBotDialog = new UserBot(userState);
const bot = new MainBot(conversationState, userState, userBotDialog);

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError] unhandled error: ${error}`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${error}`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity('To continue to run this bot, please fix the bot source code.');
    // Clear out state
    await conversationState.delete(context);
};

// create server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log(`\n${server.name} listing to ${server.url}.`);
})

// listen for incoming requests
server.post("/api/messages", (req, res) => {
    adapter.processActivity(req, res, async (context) => {
        // Run the message to main bot handler
        await bot.run(context);
    })
})