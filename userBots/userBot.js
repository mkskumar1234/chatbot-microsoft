const { MessageFactory, CardFactory, AttachmentLayoutTypes, InputHints } = require("botbuilder")
const {
    ComponentDialog,
    ChoicePrompt,
    ConfirmPrompt,
    TextPrompt,
    NumberPrompt,
    DialogSet,
    DialogTurnStatus,
    WaterfallDialog,
    Dialog } = require("botbuilder-dialogs")
const nodemailer = require("nodemailer");
const moment = require("moment");

const DialogId = require("../constants/DialogId");
const { UserProfile } = require("../stats/userProfile");

// services
const { commonService } = require("../services");

const USER_PROFILE = 'USER_PROFILE';
const CHOICE_PROMPT = 'CHOICE_PROMPT';
const CONFIRM_PROMPT = 'CONFIRM_PROMPT';
const NAME_PROMPT = 'NAME_PROMPT';
const EMAIL_PROMPT = 'EMAIL_PROMPT';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
const MOBILE_PROMPT = 'MOBILE_PROMPT';
const FULLNAME_PROMPT = 'FULLNAME_PROMPT';
const LEAVEENCASDAYSPROMPT = 'LEAVEENCASDAYSPROMPT';
const TEXTPROMPT = 'TEXTPROMPT';

class UserBot extends ComponentDialog {
    constructor(userState) {
        super(DialogId.USERDIALOG);

        this.userProfile = userState.createProperty(USER_PROFILE);

        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));
        this.addDialog(new TextPrompt(NAME_PROMPT));
        this.addDialog(new TextPrompt(EMAIL_PROMPT, this.validEmail));
        this.addDialog(new TextPrompt(MOBILE_PROMPT, this.validMobile));
        this.addDialog(new TextPrompt(FULLNAME_PROMPT, this.validFullname));
        this.addDialog(new TextPrompt(TEXTPROMPT));


        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.askLocation.bind(this),
            this.askCity.bind(this),
            this.askDepartment.bind(this),
            this.chooseDoctor.bind(this),
            this.visitingTime.bind(this),
            this.meetingSlot.bind(this),
            this.checkAlreadyRegister.bind(this),
            this.checkIsRegister.bind(this),
            this.askRegisterNo.bind(this),
            this.askFullname.bind(this),
            this.askMobile.bind(this),
            this.askGender.bind(this),
            this.askDob.bind(this),
            this.askEmail.bind(this),
            this.thankYou.bind(this)
        ]))

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    validEncasDays(promptContext) {
        let days = promptContext.context.activity.text;
        // TODO: please add one more step for max leave days number here
        return (!isNaN(days) && days > 0);
    }

    async askLocation(stepContext) {
        const options = {
            prompt: 'Which area are you looking for the Hospital in?',
            retryPrompt: 'Please chose at least one choice.',
            choices: await this.getLocationChoice()
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }
    async askCity(stepContext) {
        stepContext.values.state = stepContext.result.value;

        const options = {
            prompt: 'Which city you now?',
            retryPrompt: 'Please chose at least one choice.',
            choices: await this.getCityChoice(stepContext)
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }
    

    async askDepartment(stepContext) {
        stepContext.values.city = stepContext.result.value;
        console.log("-", stepContext.values)
        const options = {
            prompt: 'Here are the doctors in that department 👇',
            retryPrompt: 'Please chose at least one choice.',
            choices: await this.getDepartmentChoice()
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    async chooseDoctor(stepContext) {
        stepContext.values.department = stepContext.result.value;
        console.log("-", stepContext.values)
        const options = {
            prompt: 'Please select the doctor you would like to me 😊',
            retryPrompt: 'Please chose at least one choice.',
            choices: await this.getDoctorsChoice(stepContext)
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    async visitingTime(stepContext) {
        stepContext.values.doctorId = stepContext.result.value;
        console.log("-", stepContext.values);

        const options = {
            prompt: 'Are you visiting for the first time, or is it a follow-up visit?',
            retryPrompt: 'Please chose at least one choice.',
            choices: ['Yes', 'Nope']
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    async meetingSlot(stepContext) {
        stepContext.values.visitType = stepContext.result.value;
        console.log("-", stepContext.values);

        const options = {
            prompt: 'When do you prefer to meet the doctor?',
            retryPrompt: 'Please chose at least one choice.',
            choices: ['in this week', 'next 7 days', 'within 15 days']
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    // 
    async checkAlreadyRegister(stepContext) {
        stepContext.values.meetingSlot = stepContext.result.value;
        await stepContext.context.sendActivity("Got it. Let me take your information, and we are done");

        const options = {
            prompt: 'Are you already registered with us?',
            retryPrompt: 'Please chose at least one choice.',
            choices: ['yes', 'not']
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    async checkIsRegister(stepContext) {
        stepContext.values.isRegister = stepContext.result.value;
        console.log("-", stepContext.values);

        if (stepContext.result.value != 'yes') {
            return await stepContext.next();
        }
        return await stepContext.prompt(NAME_PROMPT, 'What is your register no?');

    }

    async askRegisterNo(stepContext) {
        stepContext.values.registerNo = stepContext.result;
        const options = {
            prompt: 'What is your full name?',
            retryPrompt: 'Enter full name ex. \"john smith\".'
        }

        return await stepContext.prompt(FULLNAME_PROMPT, options);
    }

    async askFullname(stepContext) {
        stepContext.values.fullname = stepContext.result;
        const options = {
            prompt: 'Your mobile number?',
            retryPrompt: 'Enter number ex. 1234567890'
        }

        return await stepContext.prompt(MOBILE_PROMPT, options);
    }

    async askMobile(stepContext) {
        stepContext.values.mobileNo = stepContext.result;
        console.log("-", stepContext.values);

        const options = {
            prompt: 'Please select your gender 👇',
            retryPrompt: 'Please select at least one',
            choices: ['Male', 'Female']
        };
        return await stepContext.prompt(CHOICE_PROMPT, options);
    }

    async askGender(stepContext) {
        stepContext.values.gender = stepContext.result.value;
        console.log("-", stepContext.values);
        const msg = 'And what\'s your date of birth?';

        await stepContext.context.sendActivity(MessageFactory.text(msg, msg, InputHints.IgnoringInput));
        await stepContext.context.sendActivity({ attachments: [this.showDateCard()] });
        return Dialog.EndOfTurn;
    }

    async askDob(stepContext) {
        const dob = stepContext.context.activity.value.dob;
        stepContext.values.dob = dob;
        if (moment(dob).isValid()) {
            let today = moment();
            if (today.diff(moment(dob), 'days') > 1) {
                const promptOptions = { prompt: 'Finally, your email address.', retryPrompt: 'Email address ex. email@email.com' };
                return await stepContext.prompt(EMAIL_PROMPT, promptOptions);
            } else {
                const msg = 'Future date not allowed?';
                await stepContext.context.sendActivity(MessageFactory.text(msg, msg, InputHints.IgnoringInput));
                return await stepContext.next(-1);
            }
        } else {
            const msg = 'Please choose valid date?';
            await stepContext.context.sendActivity(MessageFactory.text(msg, msg, InputHints.IgnoringInput));
            return await stepContext.next(-1);
        }
    }

    async askEmail(stepContext) {
        stepContext.values.email = stepContext.result;

        const moreOpt = 'Do you have any comments/messages about the appointment? Please give them here 👇';
        await stepContext.context.sendActivity(MessageFactory.text(moreOpt, moreOpt, InputHints.ExpectingInput));

        await stepContext.context.sendActivity({ attachments: [this.showSkipCard()] });
        return Dialog.EndOfTurn;
    }

    async thankYou(stepContext) {
        console.log("0stepContext", stepContext)
        console.log("activity", stepContext.context.activity)
        let thanks = "Thank you for your time, we have all the information we need 😊";
        let willBack = "You will soon hear back from our team";
        let greatDay = "Have a great day";
        await stepContext.context.sendActivity(MessageFactory.text(thanks, thanks));
        await stepContext.context.sendActivity(MessageFactory.text(willBack, willBack));
        await stepContext.context.sendActivity(MessageFactory.text(greatDay, greatDay));
        return stepContext.endDialog();
    }


    async getLocationChoice() {
        let departments = await commonService.getStates();
        const options = [];
        // console.log("-title--",departments.result)
        let result = departments.result;
        for (let index = 0; index < result.length; index++) {
            const element = result[index];

            options.push({
                value: element.title
            })
        }
        return options;
    }
    async getCityChoice(stepContext) {
        const stateId = stepContext.values.state;

        let cities = await commonService.getCityByStates(stateId);
        const options = [];
        console.log("-cities--",cities.result)
        let result = cities.result;
        for (let index = 0; index < result.length; index++) {
            const element = result[index];

            options.push({
                value: element.title
            })
        }
        return options;
    }
    

    async getDepartmentChoice() {
        let departments = await commonService.getDepartment();
        const options = [];
        console.log("-departments--",departments.result)
        let result = departments.result;
        for (let index = 0; index < result.length; index++) {
            const element = result[index];

            options.push({
                value: element.title
            })
        }
        return options;
    }

    async getDoctorsChoice(stepContext) {
        const departmentId = stepContext.values.department;
        const state = stepContext.values.state;
        const city = stepContext.values.city;
        console.log("-departmentId",departmentId);
        let doctors = await commonService.getDoctors(departmentId, state, city);
        const options = [];
        console.log("-doctors--",doctors.result)
        let result = doctors.result;
        for (let index = 0; index < result.length; index++) {
            const element = result[index];

            options.push({
                value: element.name
            })
        }
        // const options = [
        //     {
        //         value: 'kamal'
        //     },
        //     {
        //         value: 'ram'
        //     },
        //     {
        //         value: 'sunil',
        //     }
        // ]
        return options;
    }

    showDateCard() {
        return CardFactory.adaptiveCard(
            {
                "type": "AdaptiveCard",
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "version": "1.2",
                "body": [
                    {
                        "type": "Input.Date",
                        "id": "dob",
                        "value": "11-11-2015",
                        "max": "11-11-2015",
                        "validation": {
                            "necessity": "required",
                            "errorMessage": "Please fill VALID DATE"
                        }
                    }
                ],
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Ok"
                    }
                ]
            }
        )
    }

    showSkipCard() {
        return CardFactory.adaptiveCard(
            {
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "type": "AdaptiveCard",
                "version": "1.2",
                "body": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": 2,
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "text": "Type extra comments here",
                                        "weight": "Bolder",
                                        "size": "Medium"
                                    },
                                    {
                                        "type": "Container",
                                        "items": [
                                            {
                                                "type": "TextBlock",
                                                "text": "You can skip this by clicking on skip",
                                                "wrap": true
                                            },
                                            {
                                                "type": "Input.Text",
                                                "id": "remark",
                                                "placeholder": "Extra notes here",
                                                "spacing": "Padding",
                                                "separator": true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Thats It",
                        "data": {
                            "id": "Submit"
                        },
                        "style": "positive"
                    },
                    {
                        "type": "Action.Submit",
                        "title": "Skip",
                        "data": {
                            "id": "Skip"
                        },
                        "style": "destructive"
                    }
                ]
            }
        )
    }

    async sendEmailToAdmin(stepContext) {
        let testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            from: process.env.SMTP_FROM,
            host: process.env.SMTP_HOST, // hostname
            service: process.env.SMTP_SERVICE,
            auth: {
                user: process.env.SMTP_AUTH_USER,
                pass: process.env.SMTP_AUTH_PASS
            }
        });
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: process.env.SMTP_FROM, // sender address
            to: "mkskumar1234@gmail.com", // list of receivers
            subject: "Hello ✔", // Subject line
            text: "Hello world?", // plain text body
            html: "<b>Hello world?</b>", // html body
        });

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        return info;
    }

    async validEmail(promptContext) {
        if (promptContext.recognized.succeeded && promptContext.recognized.value) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(promptContext.recognized.value)) {
                return true;
            }
            return false;
        }
        return false;
    }

    async validMobile(promptContext) {
        console.log("a")
        if (promptContext.recognized.succeeded && promptContext.recognized.value) {
            const mob = promptContext.recognized.value;
            if (mob.toString().length == 10) {
                return true;
            }
            return false;
        }
        return false;
    }

    async validFullname(promptContext) {
        if (promptContext.recognized.succeeded && promptContext.recognized.value) {
            var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;
            const name = promptContext.recognized.value;
            if (regName.test(name)) {
                return true;
            }
            return false;
        }
        return false;
    }
}

module.exports.UserBot = UserBot;