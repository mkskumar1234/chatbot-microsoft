const { CardFactory } = require("botbuilder");

const showDatepicker: function(){
    return CardFactory.adaptiveCard(
        {
            "type": "AdaptiveCard",
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.2",
            "body": [
                {
                    "type": "Input.Date",
                    "id": "dob",
                    "value": "11-11-2015",
                    "max": "11-11-2015",
                    "validation": {
                        "necessity": "required",
                        "errorMessage": "Please fill Valid Date"
                    }
                }
            ],
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "Ok"
                }
            ]
        }
    )

}

module.exports = {
    showDatepicker
}



