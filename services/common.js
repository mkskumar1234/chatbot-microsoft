// const { resolve } = require("path");
// const { rejects } = require("assert");
const request = require("request");
const axios = require("axios");


module.exports = {
    getStates: async function () {
        try {
            const response = await axios.get('http://localhost:4011/states');
            console.log("response,", response.data.data);

            return { status: true, result: response.data.data };
        } catch (error) {
            return { status: false };
        }
    },
    getCityByStates: async function (state_id) {
        try {
            const response = await axios.get(`http://localhost:4011/cities/${state_id}`);
            console.log("state_id,", response.data.data);

            return { status: true, result: response.data.data };
        } catch (error) {
            return { status: false };
        }
    },
    
    getDepartment: async function () {
        try {
            const response = await axios.get('http://localhost:4011/departments');
            console.log("getDepartment,", response.data.data);

            return { status: true, result: response.data.data };
        } catch (error) {
            return { status: false };
        }
    },

    getDoctors: async function (department_id,state_id,city_id) {
        try {
            params = {
                department_id: department_id,
                state_id: state_id,
                city_id: city_id,
            }
            console.log("params,", params);

            const response = await axios.post('http://localhost:4011/doctors', params);
            console.log("getDoctors,", response.data);

            return { status: true, result: response.data.data };
        } catch (error) {
            return { status: false };
        }
    }
};