const { ActivityHandler, MessageFactory } = require("botbuilder");

class MainBot extends ActivityHandler {
    /**
     * @param {ConversationState} conversationState
     * @param {UserState} userState
     * @param {Dialog} dialog
     */

    constructor(conversationState, userState, dialog) {
        super();
        if (!conversationState) throw new Error('[DialogBot]: Missing parameter. conversationState is required');
        if (!userState) throw new Error('[DialogBot]: Missing parameter. userState is required');
        if (!dialog) throw new Error('[DialogBot]: Missing parameter. dialog is required');

        this.conversationState = conversationState;
        this.userState = userState;
        this.dialog = dialog;
        this.dialogState = this.conversationState.createProperty('DialogState');

        this.onMessage(async (context, next) => {
            console.log('Running dialog with Message Activity.');

            // Run the Dialog with the new message Activity.
            await this.dialog.run(context, this.dialogState);

            await next();
        });

        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;

            for (let index = 0; index < membersAdded.length; index++) {
                const element = membersAdded[index];
                if (element.id != context.activity.recipient.id) {
                    const welcometext = 'Hi! Welcome to UCLA Medical Centre';
                    const welcometext2 = 'Request to schedule an appointment with our expert consultants.';
                    await context.sendActivity(MessageFactory.text(welcometext, welcometext));
                    await context.sendActivity(MessageFactory.text(welcometext2, welcometext2));
                    await this.dialog.run(context, this.dialogState);
                }
            }
            await next();
        })
    }

    async run(context) {
        await super.run(context);

        // Save any state changes. The load happened during the execution of the Dialog.
        await this.conversationState.saveChanges(context, false);
        await this.userState.saveChanges(context, false);
    }
}

module.exports.MainBot = MainBot;