class UserProfile {
    constructor(location, department, doctorId, visitType, meetingSlot, isRegister, registerNo, fullname, mobileNo, dob, email, remark) {
        this.location = location;
        this.department = department;
        this.doctorId = doctorId;
        this.visitType = visitType;

        this.meetingSlot = meetingSlot;
        this.isRegister = isRegister;
        this.registerNo = registerNo;
        this.fullname = fullname;
        this.mobileNo = mobileNo;
        this.dob = dob;
        this.email = email;
        this.remark = remark;
    }
}

module.exports.UserProfile = new UserProfile;